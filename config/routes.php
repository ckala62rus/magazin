<?php
/**
 * Created by PhpStorm.
 * User: Ckala
 * Date: 24.11.2018
 * Time: 10:55
 */
return array(

    //'news/([a-z]+)/([0-9]+)' => 'news/view/$1/$2', //actionView в NewsController
    //'news/info' => 'news/info',
    //'news/([0-9]+)' => 'news/view/$1', //actionView в NewsController
    //'news' => 'news/index', //actionIndex в NewsController
    //'product' => 'product/list', //actionList в ProductController

    'product/([0-9])+' => 'product/view/$1', // actionView в ProductController

    'catalog' => 'catalog/index', // actionIndex в CatalogController

    'category/([0-9]+)/page-([0-9]+)' => 'catalog/category/$1/$2', // actionCategory в CatalogController
    'category/([0-9]+)' => 'catalog/category/$1', // actionCategory в CatalogController

    'cart/add/([0-9]+)' => 'cart/add/$1', //actionAdd в CartController
    'cart/addAjax/([0-9]+)' => 'cart/addAjax/$1', //actionAdd в CartController
    'cart' => 'cart/index', //actionIndex в CartController

    'user/register' => 'user/register',
    'user/login' => 'user/login',
    'user/logout' => 'user/logout',

    'cabinet/edit' => 'cabinet/edit',
    'cabinet' => 'cabinet/index',

    'contacts' => 'site/contact',

    '' => 'site/index', //actionIndex в SiteController
);