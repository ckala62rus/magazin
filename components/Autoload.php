<?php
/**
 * Created by PhpStorm.
 * User: Ckala
 * Date: 01.12.2018
 * Time: 9:50
 */

function __autoload($class_name)
{
    //Массив папок, где будет осуществляться поиск классов
    $array_paths = array(
        '/models/',
        '/components/',
    );

    //Перебираем массив и проверяем наличие класса и подключаем его.
    foreach ($array_paths as $path) {
        $path = ROOT . $path . $class_name . '.php';
        if (is_file($path)) {
            include_once ($path);
        }
    }
}