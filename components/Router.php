<?php
/**
 * Created by PhpStorm.
 * User: Ckala
 * Date: 24.11.2018
 * Time: 10:50
 */
class Router
{
    private $routes;

    public function __construct()
    {
        $routesPatch = ROOT.'/config/routes.php';
        $this->routes = include($routesPatch);
    }

    /**
     * Return request string
     * @return string
     */
    private function getUri()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'],'/');
        }
    }

    public function run()
    {
        // Получить строку запроса
        $uri = $this->getUri();

        //Проверка наличия такого запроса в routes.php
        foreach ($this->routes as $uriPattern => $path) {

            //Сравнение $uriPattern и $uri
            if (preg_match("~$uriPattern~",$uri)) {

                //Получаем внутренний путь из внешнего согласно правилу
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                //Определить какой контроллер и action, параметры

                $segments = explode('/',$internalRoute);

                $controllerName = array_shift($segments).'Controller';
                $controllerName = ucfirst($controllerName);

                $actionName = 'action'.ucfirst(array_shift($segments));

                $parameters = $segments;

                //Подключить файл класса-контроллера
                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';

                if (file_exists($controllerFile)) {
                    include_once ($controllerFile);
                }

                //Создать обьект, вызвать action(т.е. action)
                $controllerObject = new $controllerName;

                $result = call_user_func_array(array($controllerObject,$actionName), $parameters);

                if ($result != null) {
                    break;
                }
            }
        }

    }
}