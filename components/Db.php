<?php
/**
 * Created by PhpStorm.
 * User: Ckala
 * Date: 24.11.2018
 * Time: 13:15
 */
class Db
{
    public static function getConnection()
    {
        $paramsPath = ROOT . '/config/db_params.php';

        $params = include($paramsPath);

        $dns = "mysql:host={$params['host']};dbname={$params['dbname']}";
        $db = new PDO($dns, $params['user'], $params['password']);
        $db->exec("set names utf8");

        return $db;
    }
}