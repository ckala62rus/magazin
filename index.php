<?php
/**
 * Created by PhpStorm.
 * User: Ckala
 * Date: 24.11.2018
 * Time: 10:20
 */

//1 Фронт контроллер

//2 Общие настройки
ini_set('display_errors',1);
error_reporting(E_ALL);

session_start();

//3 Подключение файлов системы
define('ROOT',dirname(__FILE__));

//Загрузка Автозагрузчика
require_once (ROOT.'/components/Autoload.php');

//4 Установка соединения с бд

//5 Вызов Router
$router = new Router();
$router->run();
