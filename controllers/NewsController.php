<?php
/**
 * Created by PhpStorm.
 * User: Ckala
 * Date: 24.11.2018
 * Time: 11:39
 */

class NewsController
{
    public function actionIndex()
    {
        $newsList = array();
        $newsList = News::getNewsList();

        require_once (ROOT . '/views/news/index.php');

        return true;
    }

    public function actionView($category,$id)
    {
        echo $category . '<br>';
        echo $id . '<br>';
        return true;
    }

    public function actionInfo()
    {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM category';

        $result = $db->query($sql);

        $i = 0;

        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            echo '<pre>';
            print_r($row);
            $i++;
        }

        return true;
    }
}