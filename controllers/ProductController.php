<?php
/**
 * Created by PhpStorm.
 * User: Ckala
 * Date: 24.11.2018
 * Time: 11:46
 */

class ProductController
{
    public function actionView($productId)
    {
        $categories = array();
        $categories = Category::getCategoriesList();

        $product = Product::getProductById($productId);

        require_once (ROOT . '/views/product/view.php');

        return true;
    }
}