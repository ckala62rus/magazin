<?php
/**
 * Created by PhpStorm.
 * User: Ckala
 * Date: 24.11.2018
 * Time: 21:27
 */

class SiteController
{
    public function actionIndex()
    {
        $categories = array();
        $categories = Category::getCategoriesList();

        $latestProducts = array();
        $latestProducts = Product::getLatestProducts(6);

        require_once (ROOT . '/views/site/index.php');

        return true;
    }

    public function actionContact()
    {
        $userEmail  = '';
        $userText   = '';
        $result     = false;

        if (isset($_POST['submit'])) {
            $userEmail  = $_POST['userEmail'];
            $userText   = $_POST['userText'];

            $errors = false;

            //Валидация полей
            if ($errors == false) {
                $adminEmail = 'agr.akyla@mail.ru';
                $message = "Текст: {$userText}. От {$userEmail}";
                $subject = 'Тема письма';
                $result = mail($adminEmail, $subject, $message);
                $result = true;
            }
        }

        require_once (ROOT . '/views/site/contact.php');

        return true;
    }
}