<?php
/**
 * Created by PhpStorm.
 * User: Ckala
 * Date: 25.11.2018
 * Time: 20:37
 */

class CatalogController
{
    public function actionIndex()
    {
        $categories = array();
        $categories = Category::getCategoriesList();

        $latestProducts = array();
        $latestProducts = Product::getLatestProducts(9);

        require_once (ROOT . '/views/catalog/index.php');

        return true;
    }

    public function actionCategory($categoryId, $page = 1)
    {
        $categories = array();

        //Получаем массив списка категорий
        $categories = Category::getCategoriesList();

        $categoryProducts  = array();

        //Получаем массив таваров данной категории
        $categoryProducts = Product::getProductsListByCategory($categoryId, $page);

        //Количество товаров данной категории
        $total = Product::getTotalProductsInCategory($categoryId);

        //Создаем обьект класса Pagination
        $pagination = new Pagination($total, $page, Product::SHOW_BY_DEFAULT, 'page-');

        require_once (ROOT . '/views/catalog/category.php');
    }
}